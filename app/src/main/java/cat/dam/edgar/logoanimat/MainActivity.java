package cat.dam.edgar.logoanimat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private int nAnimation;
    private ImageView iv_logo;
    private Button btn_stop, btn_next;
    private ArrayList<Animation> animations;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
        setAnimation();
    }

    private void setVariables()
    {
        nAnimation = 0;
        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        btn_stop = (Button) findViewById(R.id.btn_stop);
        btn_next = (Button) findViewById(R.id.btn_next);
        setAnimationsVar();
    }

    private void setAnimationsVar()
    {
        animations = new ArrayList<>();

        Animation rotate = (Animation) AnimationUtils.loadAnimation(this, R.anim.rotate);
        Animation zoom_in = (Animation) AnimationUtils.loadAnimation(this, R.anim.zoom_in);

        animations.add(rotate);
        animations.add(zoom_in);
    }

    private void setAnimation()
    {
        iv_logo.startAnimation(animations.get(nAnimation%2));
    }

    private void setListeners()
    {
        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_logo.clearAnimation();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nAnimation++;
                setAnimation();
            }
        });
    }

}